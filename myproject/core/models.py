from django.db import models


class Demography(models.Model):
    name = models.CharField(max_length=80)

    def __str__(self):
        return self.name


class Community(models.Model):
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.ImageField()
    demography = models.ForeignKey(Demography, on_delete=models.CASCADE)
    age = models.IntegerField()

    def __str__(self):
        return self.author


class News(models.Model):
    headline = models.CharField(max_length=200)
    synopsis = models.TextField(max_length=500)
    date = models.DateField()
    byline = models.TextField(max_length=500)
    lead = models.TextField(max_length=500)
    body = models.TextField(max_length=500)
    demography = models.ForeignKey(Demography, on_delete=models.CASCADE)
    image = models.ImageField()

    def __str__(self):
        return self.headline
