from django.shortcuts import render, redirect, get_object_or_404
from .models import Community, News
from .forms import CustomUserForm, CommunityForm, NewsForm
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserChangeForm, PasswordResetForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from rest_framework import viewsets
from .serializers import CommunitySerializer, NewsSerializer


# Home View


def home(request):
    data = {
        'news': News.objects.all()
    }
    return render(request, 'core/home.html', data)


# News Views

def news(request, id):
    newp = get_object_or_404(News, id=id)
    data = {
        'news': NewsForm(instance=newp)
    }
    if request.method == 'POST':
        formulario = NewsForm(
            data=request.POST, instance=newp, files=request.FILES)
        data['news'] = formulario
    return render(request, 'core/news.html', data)


def add_news(request):
    data = {
        'form': NewsForm()
    }
    if request.method == 'POST':
        form = NewsForm(request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, "Noticia agregada correctamente")
            return redirect(to='home')
    return render(request, 'core/add_news.html', data)


def modify_news(request, id):
    news = get_object_or_404(News, id=id)

    data = {
        'form': NewsForm(instance=news)
    }
    if request.method == 'POST':
        form = NewsForm(
            data=request.POST, instance=news, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(
                request, "Noticia modificada correctamente")
            return redirect(to="list_news")
            data['form'] = form
    return render(request, 'core/modify_news.html', data)


def list_news(request):
    qs = News.objects.all()
    id_exact_query = request.GET.get('id_exact')
    headline_query = request.GET.get('headline')

    if id_exact_query != '' and id_exact_query is not None:
        qs = qs.filter(id=id_exact_query)

    elif headline_query != '' and headline_query is not None:
        qs = qs.filter(headline__icontains=headline_query)

    context = {
        'querysett': qs
    }

    return render(request, "core/list_news.html", context)


def delete_news(request, id):
    news = get_object_or_404(News, id=id)
    news.delete()
    messages.success(request, "Noticia eliminada correctamente")
    return redirect(to="list_news")


# Community Views

def galery_community(request):
    data = {
        'comunidad': Community.objects.all()
    }
    return render(request, 'core/galery_community.html', data)


def register_community(request):
    data = {
        'form': CommunityForm()
    }
    if request.method == 'POST':
        form = CommunityForm(request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(request, 'Has subido tu Post correctamente!')
            return redirect(to='galery_community')
    return render(request, 'core/register_community.html', data)


def modify_community(request, id):
    community = get_object_or_404(Community, id=id)

    data = {
        'form': CommunityForm(instance=community)
    }
    if request.method == 'POST':
        form = CommunityForm(
            data=request.POST, instance=community, files=request.FILES)
        if form.is_valid():
            form.save()
            messages.success(
                request, "Post de comunidad modificado correctamente")
            return redirect(to="list_community")
            data['form'] = form
    return render(request, 'core/modify_community.html', data)


def delete_community(request, id):
    community = get_object_or_404(Community, id=id)
    community.delete()
    messages.success(request, "Post de comunidad eliminado correctamente")
    return redirect(to="list_community")


@permission_required('core.view_community')
def list_community(request):
    qs = Community.objects.all()
    id_exact_query = request.GET.get('id_exact')
    author_query = request.GET.get('author')
    age_query = request.GET.get('age')

    if id_exact_query != '' and id_exact_query is not None:
        qs = qs.filter(id=id_exact_query)

    elif author_query != '' and author_query is not None:
        qs = qs.filter(author__icontains=author_query)

    elif age_query != '' and age_query is not None:
        qs = qs.filter(age__icontains=age_query)
    context = {
        'queryset': qs
    }

    return render(request, "core/list_community.html", context)


# Login Views

def user_register(request):
    data = {
        'form': CustomUserForm()
    }

    if request.method == 'POST':
        form = CustomUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']
            user = authenticate(username=username, password=password)
            messages.success(request, 'Te has registrado correctamente!')
            login(request, user)
            return redirect(to='home')
        else:
            return redirect(to='user_register')

    return render(request, 'registration/register.html', data)


def PasswordResetView(request):
    if request.method == 'POST':
        form = PasswordResetForm(request.user, request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)


# Serializers


class CommunityViewSet(viewsets.ModelViewSet):
    queryset = Community.objects.all()
    serializer_class = CommunitySerializer

    def get_queryset(self):
        community = Community.objects.all()
        author = self.request.GET.get('author')

        if author:
            community = community.filter(author__contains=author)
        return community


class NewsViewSet(viewsets.ModelViewSet):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
