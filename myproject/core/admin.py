from django.contrib import admin
from .models import Demography, Community, News


class CommunityAdmin(admin.ModelAdmin):
    list_display = ['author', 'description', 'age', 'demography']
    search_fields = ['author']


class NewsAdmin(admin.ModelAdmin):
    list_display = ['headline', 'synopsis', 'date',
                    'byline', 'lead', 'body', 'demography', 'image']


admin.site.register(News, NewsAdmin)
admin.site.register(Community, CommunityAdmin)
admin.site.register(Demography)
