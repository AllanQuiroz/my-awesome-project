from django.urls import path, include
from django.contrib.auth import views as auth_views
from .views import home, user_register, galery_community, register_community, PasswordResetView, news, add_news, list_community, delete_community, modify_community, modify_news, list_news, delete_news, CommunityViewSet, NewsViewSet
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView,  PasswordChangeView
from rest_framework import routers

router = routers.DefaultRouter()
router.register('community', CommunityViewSet)
router.register('news', NewsViewSet)

urlpatterns = [
    path('', home, name="home"),
    path('register/', user_register, name='user_register'),
    path('reset/password/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_form.html'), name="reset_password"
         ),
    path('reset/password/done/', auth_views.PasswordResetView.as_view(template_name='registration/password_reset_done.html'), name="reset_password_done"
         ),
    path('galery/community/', galery_community, name='galery_community'),
    path('register/community/', register_community, name="register_community"),

    path('news/<id>/', news, name="news"),
    path('add/news/', add_news, name="add_news"),
    path('list/community/', list_community, name="list_community"),
    path('delete/community/<id>/', delete_community, name="delete_community"),
    path('modify/community/<id>/',
         modify_community, name="modify_community"),
    path('modify/news/<id>/', modify_news, name="modify_news"),
    path('list/news/', list_news, name="list_news"),
    path('delete/news/<id>/', delete_news, name="delete_news"),
    path('modify/news/<id>/',
         modify_news, name="modify_news"),
    path('api/', include(router.urls)),
]
