from django import forms
from django.forms import ModelForm
from .models import Community, News
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class CustomUserForm(UserCreationForm):

    first_name = forms.CharField(min_length=3, max_length=100)
    last_name = forms.CharField(min_length=3, max_length=100)
    email = forms.EmailField(min_length=3, max_length=100)
    username = forms.CharField(min_length=3, max_length=100)
    password1 = forms.CharField(widget=forms.PasswordInput,
                                min_length=8, max_length=100)
    password2 = forms.CharField(widget=forms.PasswordInput,
                                min_length=8, max_length=100)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email',
                  'username', 'password1', 'password2']


class CommunityForm(ModelForm):

    class Meta:
        model = Community
        fields = ['author', 'description', 'image', 'age', 'demography']


class NewsForm(ModelForm):

    class Meta:
        model = News
        fields = ['headline', 'synopsis', 'date',
                  'byline', 'lead', 'body', 'demography', 'image']
