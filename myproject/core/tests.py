from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from .models import Demography, News, Community
from django.contrib.auth.models import User
# Demography & News


class DemographyTestCase(TestCase):
    # @classmethod
    def setUp(self):
        # create demography object
        d1 = Demography.objects.create(name="Example")
        # add this object into News & create a news
        News.objects.create(headline="News1", synopsis="This is a synopsis news.",
                            date="2020-11-06", byline="This is a byline news.", lead="This is a lead news.", body="This is a body news.", image="This is a image.", demography=d1)

    # @classmethod
    def test_add_news(self):
        # get News object
        news1 = News.objects.get(headline="News1")
        self.assertEqual(news1.demography.name, "Example")


class CommunityTestCase(TestCase):
    # @classmethod
    def setUp(self):
        # create demography object
        d2 = Demography.objects.create(name="Example2")
        # add this object into Community & create a Community post
        Community.objects.create(author="Post1", description="This is a description.",
                                 image="This is a image", age="2020", demography=d2)

    # @classmethod
    def test_add_post(self):
        # get Community object
        post1 = Community.objects.get(author="Post1")
        self.assertEqual(post1.demography.name, "Example2")


class NewUserTestCase(TestCase):
    # @classmethod
    def setUp(self):
        # create user object
        User.objects.create(first_name="Name", last_name="Last name",
                            email="example@email.com", username="ImaUsername", password="contrasena2112")

    def test_see_user(self):
        # get Community object
        us = User.objects.get(first_name="Name")
        self.assertEqual(us.username, "ImaUsername")
