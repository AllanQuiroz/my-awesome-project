const formulario = document.getElementById('formulario');
const inputs = document.querySelectorAll('#formulario input');
const expresiones = {
	usuario: /^[a-zA-Z]{6,12}$/, // Letras, minimo 6 y máximo 12.
	nombre: /^[a-zA-ZÀ-ÿ\s]{1,40}$/, // Letras y espacios, pueden llevar acentos.
	password: /^[a-zA-Z0-9_.-]{8,100}$/, // 8 a 100 caracteres.
	email: /^[a-zA-Z0-9_.-]+@(gmail|hotmail|yahoo|outlook)+\.(com|cl)+$/,
	telefono: /^9\d{8}$/ // 9 dígitos, comenzando con prefijo 9 de chile .
}

const campos = {
	usuario: false,
	nombre: false,
	password: false,
	email: false,
	telefono: false
}
const validarFormulario = (e) => {
	switch (e.target.name) {
		case "usuario":
			validarCampo(expresiones.usuario, e.target, e.target.name);
			break;
		case "nombre":
			validarCampo(expresiones.nombre, e.target, e.target.name);
			break;
		case "password":
			validarCampo(expresiones.password, e.target, e.target.name);
			validarPassword2();
			break;
		case "password2":
			validarPassword2();
			break;
		case "email":
			validarCampo(expresiones.email, e.target, e.target.name);
			break;
		case "telefono":
			validarCampo(expresiones.telefono, e.target, e.target.name);
			break;

	}
}

const validarCampo = (expresion, input, campo) => {
	// Texto ingresado correcto.
	if (expresion.test(input.value)) {
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-correcto');
		// Agregar icono de check.
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-check-circle');
		// Eliminar el icono de error.
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-times-circle');
		// Eliminar leyenda de error.
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos[campo] = true;
		// Texto ingresado incorrecto.
	} else {
		document.getElementById(`grupo__${campo}`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__${campo}`).classList.remove('formulario__grupo-correcto');
		// Agregar icono de error.
		document.querySelector(`#grupo__${campo} i`).classList.add('fa-times-circle');
		// Eliminar el icono de check.
		document.querySelector(`#grupo__${campo} i`).classList.remove('fa-check-circle');
		// Mostrar leyenda de error.
		document.querySelector(`#grupo__${campo} .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos[campo] = false;
	}
}

const validarPassword2 = () => {
	const inputPassword1 = document.getElementById('password');
	const inputPassword2 = document.getElementById('password2');

	if (inputPassword1.value !== inputPassword2.value) {
		document.getElementById(`grupo__password2`).classList.add('formulario__grupo-incorrecto');
		document.getElementById(`grupo__password2`).classList.remove('formulario__grupo-correcto');
		// Agregar icono de error.
		document.querySelector(`#grupo__password2 i`).classList.add('fa-times-circle');
		// Eliminar el icono de check.
		document.querySelector(`#grupo__password2 i`).classList.remove('fa-check-circle');
		// Mostrar leyenda de error.
		document.querySelector(`#grupo__password2 .formulario__input-error`).classList.add('formulario__input-error-activo');
		campos['password'] = false;
	} else {
		document.getElementById(`grupo__password2`).classList.remove('formulario__grupo-incorrecto');
		document.getElementById(`grupo__password2`).classList.add('formulario__grupo-correcto');
		// Agregar icono de error.
		document.querySelector(`#grupo__password2 i`).classList.remove('fa-times-circle');
		// Eliminar el icono de check.
		document.querySelector(`#grupo__password2 i`).classList.add('fa-check-circle');
		// Mostrar leyenda de error.
		document.querySelector(`#grupo__password2 .formulario__input-error`).classList.remove('formulario__input-error-activo');
		campos['password'] = true;
	}
}
// Validación por cada input.
inputs.forEach((input) => {
	input.addEventListener('keyup', validarFormulario);
	input.addEventListener('blur', validarFormulario);

});

formulario.addEventListener('submit', (e) => {
	// Evento que evita que usuario envíe formulario
	e.preventDefault();
	const terminos = document.getElementById('terminos');
	if (campos.usuario && campos.nombre && campos.password && campos.email && campos.telefono) {
		formulario.reset();
		document.getElementById('formulario__mensaje-exito').classList.add('formulario__mensaje-exito-activo');
		setTimeout(() => {
			document.getElementById('formulario__mensaje-exito').classList.remove('formulario__mensaje-exito-activo');
		}, 1500);
		document.querySelectorAll('.formulario__grupo-correcto').forEach((icono) => {
			icono.classList.remove('formulario__grupo-correcto');
		});
	} else {
		document.getElementById('formulario__mensaje').classList.add('formulario__mensaje-activo');
		setTimeout(() => {
			document.getElementById('formulario__mensaje').classList.remove('formulario__mensaje-activo');
		}, 5000);
	}
});