function confirmarEliminacion(id){
    Swal.fire({
    title: '¿Estas seguro?',
    text: "No podrás deshacer esta acción",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sí, Eliminar!',
    cancelButtonText: 'Cancelar'
}).then((result) => {
    if(result.value){
        window.location.href = "/eliminar-manga/"+id+"/"; 
    }
})
}