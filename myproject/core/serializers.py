from rest_framework import serializers
from .models import Community, Demography, News


class DemographySerializer(serializers.ModelSerializer):
    class Meta:
        model = Demography
        fields = '__all__'


class CommunitySerializer(serializers.ModelSerializer):
    name_demography = serializers.CharField(
        read_only=True, source="demography.name")
    demography = DemographySerializer(read_only=True)
    demography_id = serializers.PrimaryKeyRelatedField(
        queryset=Demography.objects.all(), source="demography")
    author = serializers.CharField(required=True, min_length=2)

    def validate_author(self, value):
        exist = Community.objects.filter(author__iexact=value).exists()

        if exist:
            raise serializers.ValidationError("Este nombre ya está en uso")

        return value

    class Meta:
        model = Community
        fields = '__all__'


class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'
