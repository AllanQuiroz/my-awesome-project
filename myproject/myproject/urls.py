from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('core.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('', include('pwa.urls')),
]

admin.site.site_header = "Administración de MyManga"
admin.site.index_title = "Modulos de administración"
admin.site.site_title = "MyManga"

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
